# e-Money Mainnet on MANTRA DAO

- Chain ID: `emoney-3`

## Submodules

- [e-money/em-ledger](https://github.com/e-money/em-ledger.git) @ `v1.2.0`
